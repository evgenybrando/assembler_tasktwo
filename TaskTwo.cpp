﻿#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <windows.h>

struct Matrix {
	int** arr;
	int n;
};
/*Среди строк матрицы определить те строки, которые являются перестановкой
     из n символов, где n --- размерность матрицы.*/
void create(Matrix*& m, int n) {
	const char mes[] = "Wrong dim!";
	__asm {
		mov ebx, n
		cmp ebx, 0
		jle EXC
		mov ebx, m
		mov ebx, [ebx]
		cmp ebx, 0
		jne CLEAR
		jmp CREATE
		CLEAR :
		push ebx
			call malloc
			add esp, 4
			CREATE :
			mov eax, 4
			mov ecx, n
			imul ecx
			push eax
			call malloc
			add esp, 4
			xor esi, esi
			mov ecx, n
			mov ebx, eax
			FORI :
		cmp esi, ecx
			jge AFTER_CYCLE
			mov eax, 4
			mul ecx
			push esi
			push ecx
			push ebx
			push eax
			call malloc
			add esp, 4
			pop ebx
			pop ecx
			pop esi
			mov[ebx][esi * 4], eax
			xor edi, edi
			FORJ :
		cmp edi, ecx
			jge NEXT
			mov dword ptr[eax][edi * 4], 0
			inc edi
			jmp FORJ
			NEXT :
		inc esi
			jmp FORI
			AFTER_CYCLE :
		mov eax, 8
			push ebx
			push eax
			call malloc
			add esp, 4
			pop ebx
			mov[eax], ebx
			mov ecx, n
			mov[eax + 4], ecx
			mov ebx, m
			mov[ebx], eax
			jmp FIN
			EXC :
		lea eax, mes
			push eax
			call printf
			add esp, 4
			FIN :
	}
}

void clear(Matrix*& m) {
	__asm {
		mov eax, m
		mov eax, [eax]
		cmp eax, 0
		je FIN
		; mov eax, [eax]
		push eax
		mov ecx, [eax + 4]
		mov eax, [eax]
		push eax
		CYCLE :
		pop eax
			mov ebx, [eax][ecx * 4 - 4]
			push eax
			push ecx
			push ebx
			call free
			add esp, 4
			pop ecx
			loop CYCLE
			call free
			add esp, 4
			call free
			add esp, 4
			mov eax, m
			mov dword ptr[eax], 0
			FIN:
	}
}

void printStringsNumber(Matrix* m, int size) {
	const char mes[] = "Matrix is absent!";
	const char pr[] = "str %d\n";
	char *pos = new char[size];
	_asm {
		mov ebx, m
		cmp ebx, 0
		je EXC
		xor esi, esi
		dec esi
		mov ecx, [ebx + 4]
		mov ebx, [ebx]
		FORI :
			inc esi
		    cmp esi, ecx
			jge FIN
			xor edi, edi
			mov edx, pos
		FORPOS :
			mov [edx][edi], 0
			inc edi
			cmp edi, ecx
			jl FORPOS
			xor edi, edi
		FORJ :
		    cmp edi, ecx
			jge NEXT
			mov eax, [ebx][esi * 4]
			mov eax, [eax][edi * 4]
			cmp eax, 0
			jle FORI
			cmp eax, ecx
			jg FORI
			dec eax
			mov [edx][eax], 1
			inc edi
		    jmp FORJ
		NEXT :
			xor edi, edi
		FORPOS1 :
			cmp [edx][edi], 1
			jne FORI
			inc edi
			cmp edi, ecx
			jl FORPOS1
			push ecx
			push ebx
			lea eax, pr
			push esi
			push eax
			call printf
			add esp, 4
			pop esi
			pop ebx
			pop ecx
			jmp FORI
		EXC :
		    lea eax, mes
			push eax
			call printf
			add esp, 4
		FIN :
	}
}

int get(Matrix* m, int i, int j) {
	const char mes1[] = "Matrix is absent!";
	const char mes2[] = "Wrong indexes!";
	__asm {
		mov eax, m
		cmp eax, 0
		je EXC_ABS
		mov esi, i
		mov edi, j
		mov edx, [eax + 4]
		cmp esi, 0
		jl ECX_IND
		cmp esi, edx
		jge ECX_IND
		mov ecx, j
		cmp edi, 0
		jl ECX_IND
		cmp edi, edx
		jge ECX_IND
		mov eax, [eax]
		mov eax, [eax][esi * 4]
		mov eax, [eax][edi * 4]
		jmp FIN
		EXC_ABS :
		lea eax, mes1
			push eax
			call printf
			add esp, 4
			mov eax, 0
			jmp FIN
			ECX_IND :
		lea eax, mes2
			push eax
			call printf
			add esp, 4
			mov eax, 0
			FIN :
	}
}

void set(Matrix* m, int i, int j, int elem) {
	const char mes1[] = "Matrix is absent!";
	const char mes2[] = "Wrong indexes!";
	__asm {
		mov eax, m
		cmp eax, 0
		je EXC_ABS
		mov esi, i
		mov edi, j
		mov edx, [eax + 4]
		cmp esi, 0
		jl ECX_IND
		cmp esi, edx
		jge ECX_IND
		mov ecx, j
		cmp edi, 0
		jl ECX_IND
		cmp edi, edx
		jge ECX_IND
		mov ebx, elem
		mov eax, [eax]
		mov eax, [eax][esi * 4]
		mov[eax][edi * 4], ebx
		jmp FIN
		EXC_ABS :
		lea eax, mes1
			push eax
			call printf
			add esp, 4
			jmp FIN
			ECX_IND :
		lea eax, mes2
			push eax
			call printf
			add esp, 4
			FIN :
	}
}

void print(Matrix* m) {
	const char mes[] = "Matrix is absent!";
	const char frt[] = "%d ";
	const char n[] = "\n";
	__asm {
		mov eax, m
		cmp eax, 0
		je EXC
		xor esi, esi
		mov ecx, [eax + 4]
		mov ebx, [eax]
		FORI :
		cmp esi, ecx
			jge FIN
			xor edi, edi
			FORJ :
		cmp edi, ecx
			jge NEXT
			push esi
			push edi
			push ecx
			push ebx
			mov eax, [ebx][esi * 4]
			mov eax, [eax][edi * 4]
			push eax
			lea eax, frt
			push eax
			call printf
			add esp, 8
			pop ebx
			pop ecx
			pop edi
			pop esi
			inc edi
			jmp FORJ
		NEXT :
			push esi
			push ecx
			push ebx
			lea eax, n
			push eax
			call printf
			add esp, 4
			pop ebx
			pop ecx
			pop esi
			inc esi
			jmp FORI
		EXC :
			lea eax, mes
			push eax
			call printf
			add esp, 4
			FIN :
	}
}

int main()
{
	const int size = 1;
	char code;
	Matrix* m = nullptr;
	do {
		printf("Menu:\n");
		printf("1. Create\n");
		printf("2. Clear\n");
		printf("3. Get\n");
		printf("4. Set\n");
		printf("5. Print strings number where elem eq numbers from one to size \n");
		printf("6. Print\n");
		printf("Press 'e' for exit");
		code = _getch();
		system("cls");
		if (code == '1') {
			create(m, size);
			_getch();
			system("cls");
		}
		else if (code == '2') {
			clear(m);
			_getch();
			system("cls");
		}
		else if (code == '3') {
			printf("%d ", get(m, 5, 5));
			_getch();
			system("cls");
		}
		else if (code == '4') {
			if (m != nullptr)
			{
				m->arr[0][0] = 1;
				/*m->arr[0][0] = 5;*/
				/*m->arr[0][0] = 1;
				m->arr[0][1] = 2;
				m->arr[0][2] = 3;
				m->arr[1][0] = 1;
				m->arr[1][1] = 2;
				m->arr[1][2] = 1;
				m->arr[2][0] = 2;
				m->arr[2][1] = 1;
				m->arr[2][2] = 3;*/
			}
			/*for (int i = 0; i < m->n; i++) {
				for (int j = 0; j < m->n; j++) {
						set(m, i, j, i * m->n + j + 1);
				}
			}*/

		}
		else if (code == '5') {
			printStringsNumber(m, size);
			_getch();
			system("cls");
		}
		else if (code == '6') {
			print(m);
			_getch();
			system("cls");
		}
	} while (code != 'e');
	return 0;
}
